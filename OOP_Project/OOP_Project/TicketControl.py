import ctypes
lib_tickets = ctypes.CDLL('./libtickets.dll')
lib_tickets = ctypes.CDLL('./DLL_Project.dll')

# checking whether the class "Ticket_Amount" works correctly
ticket_a1 = lib_tickets.Create_Ticket_A(1, 614, 60)
number = lib_tickets.Get_Id_A(ticket_a1)
sell_time = lib_tickets.Get_Sell_Time_A(ticket_a1)
max_trips = lib_tickets.Get_Max_Trips(ticket_a1)
left_trips = lib_tickets.Get_Left_Trips(ticket_a1)
lib_tickets.Try_Pass_Control_A(ticket_a1)
print(number, sell_time, max_trips, left_trips)
lib_tickets.Print_A(ticket_a1)

ticket_a0 = lib_tickets.Create_Ticket_A(4, 52, 1)
ticket_a2 = lib_tickets.Create_Ticket_A(2, 615, 0)
ticket_a3 = lib_tickets.Create_Ticket_A(8, 619, 60)

# checking whether the class "Ticket_Period" works correctly
ticket_p1 = lib_tickets.Create_Ticket_P(5, 685, 90)
number = lib_tickets.Get_Id_P(ticket_p1)
sell_time = lib_tickets.Get_Sell_Time_P(ticket_p1)
validity = lib_tickets.Get_Validity(ticket_p1)
print(number, sell_time, validity)

ticket_p0 = lib_tickets.Create_Ticket_P(34, 1, 30)
ticket_p2 = lib_tickets.Create_Ticket_P(7, 640, 50)
ticket_p3 = lib_tickets.Create_Ticket_P(6, 795, 30)

# checking whether the class "Pass_Control" works correctly
control = lib_tickets.Create_Pass_Control(800, ticket_a3)
time = lib_tickets.Get_Current_Time(control)
print(time)
print()
lib_tickets.Try_Pass_Control(control)
lib_tickets.Print_A(ticket_a3)

# checking whether the class "Known_Tickets" works correctly
tree = lib_tickets.Create_Tree()
lib_tickets.Add_Ticket(tree, ticket_p0)
lib_tickets.Add_Ticket(tree, ticket_a2)
lib_tickets.Add_Ticket(tree, ticket_a1)
lib_tickets.Add_Ticket(tree, ticket_a0)
lib_tickets.Add_Ticket(tree, ticket_p1)
lib_tickets.Add_Ticket(tree, ticket_p3)
lib_tickets.Add_Ticket(tree, ticket_p2)
lib_tickets.Add_Ticket(tree, ticket_a3)
if lib_tickets.Find_Ticket(tree, ticket_p3):
    print("OK")
lib_tickets.Print_Tree(tree)
lib_tickets.Control(tree, 644, ticket_p0)
if lib_tickets.Find_Ticket(tree, ticket_p0):
    print("OK")
lib_tickets.Print_Tree(tree)
