#ifndef PASS_CONTROL_H
#define PASS_CONTROL_H

#include "Tickets.h"

class Pass_Control {
	Ticket* ticket;  //the ticket that is presented
	int current_time;  //current time (access time)

public:
	//create class Pass_Control with the specified parameters
	Pass_Control(int current_time, Ticket* ticket) {
		if (current_time < 0) throw std::invalid_argument("Negative current time value |Create_Pass_Control|");
		if (!ticket) throw std::invalid_argument("The pointer to an existing ticket isn't defined |Create_Pass_Control|");
		this->current_time = current_time;
		this->ticket = ticket;
	}

	//get current time
	int Get_Current_Time() const {
		return current_time;
	}

	//attempt to pass control
	bool Try_Pass_Control() {
		return ticket->Try_Pass_Control(current_time);
	}
};

#endif //PASS_CONTROL_H