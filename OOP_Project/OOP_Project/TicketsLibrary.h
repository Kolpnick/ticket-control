#pragma once

#include "TreeIterator.h"

#ifdef PROJECT_EXPORTS
#define PROJECT_API __declspec(dllexport)
#else
#define PROJECT_API __declspec(dllimport)
#endif

extern "C" {
	PROJECT_API Ticket_Amount* Create_Ticket_A(int id, int sell_time, int max_trips); 
	PROJECT_API int Get_Id_A(Ticket_Amount* t);
	PROJECT_API int Get_Sell_Time_A(Ticket_Amount* t);
	PROJECT_API int Get_Max_Trips(Ticket_Amount* t);
	PROJECT_API int Get_Left_Trips(Ticket_Amount* t);
	PROJECT_API bool Try_Pass_Control_A(Ticket_Amount* t, int current_time);
	PROJECT_API void Print_A(Ticket_Amount* t);
	PROJECT_API void Dispose_Ticket_A(Ticket_Amount* t);
	PROJECT_API Ticket_Period* Create_Ticket_P(int id, int sell_time, int validity);
	PROJECT_API int Get_Id_P(Ticket_Period* t);
	PROJECT_API int Get_Sell_Time_P(Ticket_Period* t);
	PROJECT_API int Get_Validity(Ticket_Period* t);
	PROJECT_API bool Try_Pass_Control_P(Ticket_Period* t, int current_time);
	PROJECT_API void Print_P(Ticket_Period* t);
	PROJECT_API void Dispose_Ticket_P(Ticket_Period* t);
	PROJECT_API Pass_Control* Create_Pass_Control(int current_time, Ticket* ticket);
	PROJECT_API int Get_Current_Time(Pass_Control* p);
	PROJECT_API bool Try_Pass_Control(Pass_Control* p);
	PROJECT_API void Dispose_Control(Pass_Control* p);
	PROJECT_API Known_Tickets* Create_Tree();
	PROJECT_API void Print_Tree(Known_Tickets* tree);
	PROJECT_API int Add_Ticket(Known_Tickets* tree, Ticket* ticket);
	PROJECT_API Ticket* Find_Ticket(Known_Tickets* tree, int id);
	PROJECT_API int Control(Known_Tickets* tree, int current_time, Ticket* ticket);
	PROJECT_API bool Delete_Ticket(Known_Tickets* tree, Ticket* ticket);
	PROJECT_API void Dispose_Tree(Known_Tickets* tree);
	PROJECT_API Tree_Iterator* Create_Iterator(Known_Tickets* database);
	PROJECT_API int Next(Tree_Iterator* iterator);
	PROJECT_API bool Has_Next(Tree_Iterator* iterator);
	PROJECT_API void Dispose_Iterator(Tree_Iterator* iterator);
}