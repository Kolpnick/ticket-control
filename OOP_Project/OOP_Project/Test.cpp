#include <cassert>
#include "TreeIterator.h"

void Test_Tickets() {
	//creating a ticket for the set amount of trips
	Ticket_Amount ticket_a1(1, 614, 60);
	//checking whether the class "ticket_a1" was created correctly
	assert(ticket_a1.Get_Id() == 1);
	assert(ticket_a1.Get_Sell_Time() == 614);
	assert(ticket_a1.Get_Max_Trips() == 60);
	assert(ticket_a1.Get_Left_Trips() == 60);
	ticket_a1.Print();
	//trying to pass the control with the remaining trips
	assert(ticket_a1.Try_Pass_Control(734) == true);
	assert(ticket_a1.Get_Left_Trips() == 59);
	//trying to pass the control with zero remaining trips
	Ticket_Amount ticket_a2(2, 615, 0);
	assert(ticket_a2.Try_Pass_Control(636) == false);
	//trying to pass the control with one remaining trips
	Ticket_Amount ticket_a0(4, 52, 1);
	assert(ticket_a0.Try_Pass_Control(53) == true);
	//compare tickets by number
	assert(ticket_a1 < ticket_a2);


	//creating a ticket with a limited validity period
	Ticket_Period ticket_p1(5, 685, 90);
	//checking whether the class "ticket_p1" was created correctly
	assert(ticket_p1.Get_Id() == 5);
	assert(ticket_p1.Get_Sell_Time() == 685);
	assert(ticket_p1.Get_Validity() == 90);
	ticket_p1.Print();
	//trying to pass the control with the remaining validity period
	assert(ticket_p1.Try_Pass_Control(690) == true);
	//trying to pass the control with the expired validity period
	Ticket_Period ticket_p2(7, 640, 50);
	assert(ticket_p2.Try_Pass_Control(691) == false);


	Ticket_Amount ticket_a3(8, 619, 60);  //a ticket for the set amount of trips
	//creating a control pass class
	Pass_Control control1(800, &ticket_a3);
	//checking whether the class "control1" was created correctly
	assert(control1.Get_Current_Time() == 800);
	//passing the control with the remaining trips
	assert(control1.Try_Pass_Control() == true); 
	assert(ticket_a3.Get_Left_Trips() == 59);

	Ticket_Period ticket_p3(6, 795, 30);  //a ticket with a limited validity period
	//creating a new control pass class
	Pass_Control control2(803, &ticket_p3);
	//checking whether the class "control2" was created correctly
	assert(control2.Get_Current_Time() == 803);
	//passing the control with the remaining validity period
	assert(control2.Try_Pass_Control() == true);


	//creating and filling in the AVL-tree
	Ticket_Period ticket_p(185, 1, 29);
	Known_Tickets Tree;

	Tree.Find_Ticket(ticket_p.Get_Id());

	Tree.Add_Ticket(&ticket_p);
	Tree.Add_Ticket(&ticket_a2);
	Tree.Add_Ticket(&ticket_a1);
	Tree.Add_Ticket(&ticket_a0);
	Tree.Add_Ticket(&ticket_p1);
	Tree.Add_Ticket(&ticket_p3);
	Tree.Add_Ticket(&ticket_p2);
	Tree.Add_Ticket(&ticket_a3);
	//checking whether the ticket "ticket_p3" was added to the tree
	assert(Tree.Find_Ticket(ticket_p3.Get_Id()) == &ticket_p3);
	//printing the tree to the console
	Tree.Print_Tree();
	//deleting the ticket "ticket_a0" from the tree and printing the tree to the console (the root must be removed)
	Tree.Delete_Ticket(&ticket_a0);
	Tree.Print_Tree();
	//checking whether the ticket "ticket_a0" was deleted from the tree
	assert(Tree.Find_Ticket(ticket_a0.Get_Id()) == nullptr);

	//trying to pass the control with the ticket "ticket_p" (this souldn't work because the ticket has expired) => deleting the ticket from the tree
	assert(Tree.Control(644, &ticket_p) == -2);
	//checking whether the ticket "ticket_p" was deleted from the tree
	assert(Tree.Find_Ticket(ticket_p.Get_Id()) == nullptr);
	Tree.Print_Tree();
	
	//checking whether the iterator works correctly
	Tree_Iterator iterator(&Tree);
	while (iterator.Has_Next()) {
		printf("%d ", iterator.Next());
	}
}