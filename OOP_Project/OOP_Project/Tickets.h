#ifndef TICKETS_H
#define TICKETS_H

#include "stdio.h"
#include "iostream"

class Ticket {
	int id;  //ticket id
	int sell_time;  //time of ticket sell

public:
	//create class Ticket with the specified parameters
	Ticket(int id, int sell_time) {
		if (id < 0) throw std::invalid_argument("Negative id value ||Create_Ticket_A| or |Create_Ticket_P||");
		if (sell_time < 0) throw std::invalid_argument("Negative sell time value ||Create_Ticket_A| or |Create_Ticket_P||");
		this->id = id;
		this->sell_time = sell_time;
	}

	//get ticket id
	int Get_Id() const {
		return id;
	}

	//get time of ticket sell
	int Get_Sell_Time() const {
		return sell_time;
	}

	//operator == overdrive
	virtual bool operator == (const Ticket& ticket) const {
		return this->id == ticket.id;
	}

	//operator > overdrive
	virtual bool operator < (const Ticket& ticket) const {
		return this->id < ticket.id;
	}

	//virtual method of control passing, built in derived classes
	virtual bool Try_Pass_Control(int current_time) = 0;

	//virtual method for displaying ticket information in the console
	virtual void Print() const = 0;

	//virtual destructor
	virtual ~Ticket() {}
};

class Ticket_Amount: public Ticket{
	int max_trips;  //max trip amount
	int left_trips;  //left trips amount

public:
	//create calss Ticket_Amount with the specified parameters
	Ticket_Amount(int id, int sell_time, int max_trips) : 
		Ticket(id, sell_time) {
		if (max_trips < 0) throw std::invalid_argument("Negative max trips value |Create_Ticket_A|");
		this->max_trips = max_trips;
		this->left_trips = max_trips;
	}

	//get max trips amount
	int Get_Max_Trips() const {
		return max_trips;
	}
	//get left trips amount
	int Get_Left_Trips() const {
		return left_trips;
	}

	//attempt to pass control
	bool Try_Pass_Control(int current_time) {
		try {
			if (current_time < 0) throw std::invalid_argument("Negative current time value |Try_Pass_Control_A|");
			return left_trips-- > 0;
		}
		catch (std::invalid_argument) {
			return false;
		}
	}

	//display ticket information in the console
	void Print() const {
		printf("Id: %d\nSell Time: %d\nMax trips: %d\nLeft trips: %d\n\n", Get_Id(), Get_Sell_Time(), Get_Max_Trips(), Get_Left_Trips());
	}
};

class Ticket_Period : public Ticket {
	int validity;  //the validity of the ticket

public:
	//create calss Ticket_Period with the specified parameters
	Ticket_Period(int id, int sell_time, int validity) :
		Ticket(id, sell_time) {
		if (validity < 0) throw std::invalid_argument("Negative ticket validity value |Create_Ticket_P|");
		this->validity = validity;
	}

	//get the validity of the ticket
	int Get_Validity() const {
		return validity;
	}

	//attempt to pass control
	bool Try_Pass_Control(int current_time) {
		try {
			if (current_time < 0) throw std::invalid_argument("Negative current time value |Try_Pass_Control_P|");
			return (Get_Sell_Time() + validity) > current_time;
		}
		catch (std::invalid_argument) {
			return false;
		}
	}

	//display ticket information in the console
	void Print() const {
		printf("Id: %d\nSell Time: %d\nValid period: %d\n\n", Get_Id(), Get_Sell_Time(), Get_Validity());
	}
};

#endif //TICKETS_H