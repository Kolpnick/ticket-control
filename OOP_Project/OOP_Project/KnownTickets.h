#ifndef KNOWN_TICKETS_H
#define KNOWN_TICKETS_H

#include "PassControl.h"
#include "stdio.h"

const int number_of_spaces = 10; //number of spaces when print tree

//AVL-tree
struct node {
	Ticket* ticket;  //the ticket itself
	int key;  //key - the ticket id
	int height;  //the height of the subtree
	node* left;  //pointer to the left subtree
	node* right;  //pointer to the right subtree
};

class Known_Tickets {
	friend class Tree_Iterator; //making the class Tree_Iterator friendly to this

	node *tree = nullptr;  //structure that stores data about the tickets

public:
	//printing the tree to the console
	void Print_Tree() {
		Print_2DTree(tree, 0);
	}

	//adding a ticket to the tree
	int Add_Ticket(Ticket* ticket) {
		try {
			if (!ticket) throw std::invalid_argument("The pointer to an existing ticket isn't defined |Add_Ticket|");
			if (tree) {
				node* root = Find_Key(tree, ticket->Get_Id());
				if (root) throw std::logic_error("Trying to add a ticket that is already in the tree |Add_Ticket|");
			}
			tree = Add_Key(tree, ticket);
			return 0; //ticket was added
		}
		catch (std::invalid_argument) {
			return -1; //invalid argument
		}
		catch (std::logic_error) {
			return -2; //ticket with the same id is already in the tree
		}
	}

	//search for a ticket in the tree
	Ticket* Find_Ticket(int ticket_id) {
		try {
			if (tree) {
				if (ticket_id < 0) throw std::invalid_argument("Negative id value |Find_Ticket|");
				node* root = Find_Key(tree, ticket_id);
				if (root) return (root->ticket);
				else return nullptr;
			}
			return nullptr;
		}
		catch (std::invalid_argument) {
			return nullptr;
		}

	}

	//control passage
	int Control(int current_time, Ticket* ticket) {
		try {
			if (current_time < 0) throw std::invalid_argument("The pointer to an existing ticket isn't defined |Control|");
			if (!ticket) throw std::invalid_argument("The pointer to an existing ticket isn't defined |Control|");
			node* root = Find_Key(tree, ticket->Get_Id());
			if (root) {
				Pass_Control control(current_time, ticket);
				if (!control.Try_Pass_Control()) {
					this->Delete_Ticket(ticket);
					return -2; //ticket isn't valid
				}
				return 1; //access allowed
			}
			return -3; //ticket wasn't found
		}
		catch (std::invalid_argument) {
			return -1; //invalid arguments
		}
	}

	//deleting a ticket from the tree
	bool Delete_Ticket(Ticket* ticket) {
		try {
			if (!ticket) throw std::invalid_argument("The pointer to an existing ticket isn't defined |Delete_Ticket|");
			tree = Delete_Key(tree, ticket->Get_Id());
			return true;
		}
		catch (std::invalid_argument) {
			return false;
		}
	}

	//class destructor
	~Known_Tickets() {
		Delete_Tree(tree);
	}


private:
	//get tree database
	node* Get_Tree() {
		return tree;
	}

	//printing the key of each tree
	void Print_2DTree(node* root, int space) {
		if (root == nullptr)
			return;
		space += number_of_spaces;
		Print_2DTree(root->right, space);
		printf("\n");
		for (int i = number_of_spaces; i < space; i++)
			printf(" ");
		printf("%d\n", root->key);
		Print_2DTree(root->left, space);
	}

	//the calculation of the hight of the tree
	void Height_Calculation(node* root) {
		int height_l = 0, height_r = 0;
		if (root->left)
			height_l = root->left->height;
		if (root->right)
			height_r = root->right->height;
		if (height_l < height_r)
			height_l = height_r;
		root->height  = height_l + 1;
	}

	//the calculation of the hight difference of the right and left subtrees
	int Height_Difference(node* root) {
		int height_l = 0, height_r = 0;
		if (root->left)
			height_l = root->left->height;
		if (root->right)
			height_r = root->right->height;
		return height_r - height_l;
	}

	//right rotation of the tree
	node* Right_Rotation(node* root) {
		node* r = root->left;
		root->left = r->right;
		r->right = root;
		Height_Calculation(root);
		Height_Calculation(r);
		return r;
	}

	//left rotation of the tree
	node* Left_Rotation(node* root) {
		node* l = root->right;
		root->right = l->left;
		l->left = root;
		Height_Calculation(root);
		Height_Calculation(l);
		return l;
	}

	//balancing a "root" node
	node* Tree_Balance(node* root) {
		Height_Calculation(root);
		if (Height_Difference(root) == -2) {
			if (Height_Difference(root->left) > 0)
				root->left = Left_Rotation(root->left);
			return Right_Rotation(root);
		}
		if (Height_Difference(root) == 2) {
			if (Height_Difference(root->right) < 0)
				root->right = Right_Rotation(root->right);
			return Left_Rotation(root);
		}
		return root;
	}

	//adding a node with the key "key" to the tree with the root "root"
	node* Add_Key(node* root, Ticket* ticket) {
		if (root == nullptr) {
			node* b = new node;
			b->ticket = ticket;
			b->key = ticket->Get_Id();
			b->left = b->right = 0;
			b->height = 1;
			return b;
		}
		if (ticket->Get_Id() < root->key)
			root->left = Add_Key(root->left, ticket);
		else
			root->right = Add_Key(root->right, ticket);
		return Tree_Balance(root);
	}

	//deleting a node with a key "key" from the tree with the root "root"
	node* Delete_Key(node* root, int key) {
		if (root == nullptr) return 0;
		if (key < root->key)
			root->left = Delete_Key(root->left, key);
		else if (key > root->key)
			root->right = Delete_Key(root->right, key);
		else {
			node* l = 0;
			node* r = 0;
			if (root->left)
				l = root->left;
			if (root->right)
				r = root->right;
			delete root;
			if (r == nullptr) 
				return l;
			node* min = Find_Min(r);
			min->right = Delete_Min(r);
			min->left = l;
			return Tree_Balance(min);
		}
		return Tree_Balance(root);
	}
	
	//search for a node with the min key in the tree with the root "root"
	node* Find_Min(node* root) {
		if (root == nullptr)
			return 0;
		if (root->left)
			return Find_Min(root->left);
		return root;
	}

	//deleting a node with a min key from the tree with the root "root"
	node* Delete_Min(node* root) {
		if (root->left == 0)
			return root->right;
		root->left = Delete_Min(root->left);
		return Tree_Balance(root);
	}

	//search for a node with the key "key" in the tree with the root "root"
	node* Find_Key(node* root, int key) {
		if (&root == nullptr)
			return nullptr;
		if (key < root->key) {
			if (root->left)
				return Find_Key(root->left, key);
			else return nullptr;
		}
		else if (key > root->key) {
			if (root->right)
				return Find_Key(root->right, key);
			else return nullptr;
		}
		else if (key == root->key) {
			return root;
		}
	}

	//the deletion of the entire tree
	void Delete_Tree(node* root) {
		if (root == nullptr)
			return;
		Delete_Tree(root->right);
		Delete_Tree(root->left);
		delete root;
	}
};

#endif //KNOWN_TICKETS_H