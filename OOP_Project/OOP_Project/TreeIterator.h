#ifndef TREE_ITERATOR_H
#define TREE_ITERATOR_H

#include "KnownTickets.h"

class Tree_Iterator {
	int* values; //an array that stores all tree keys
	int index; //a number that indicates the current position in the array "values"
	int length; //length of database

public:
	//class Tree_Iterator constructor
	Tree_Iterator(Known_Tickets* database) {
		if (!database) throw std::invalid_argument("The pointer to an existing database isn't defined |Create_Tree_Iterator|");
		if (!database->Get_Tree()) throw std::logic_error("The database is empty |Create_Tree_Iterator|");
		this->length = Get_Tree_Size(database->Get_Tree());
		this->values = new int[this->length];
		int i = 0;
		Make_Array(database->Get_Tree(), this->values, i);
		this->index = -1;
	}

	//returning the next element in structure
	int Next() {
		try {
			if (values && this->Has_Next()) {
				this->index += 1;
				return this->values[this->index];
			}
			else
				throw std::logic_error("No more element|Next|");
		}
		catch (std::logic_error) {
			return -1;
		}
	}

	//checking whether the next element exists
	bool Has_Next() {
		if (values) {
			if (((this->index) + 1) < this->length)
				return true;
			else {
				delete values;
				return false;
			}
		}
		else 
			return false;
	}

private:
	//counting the size of structure
	int Get_Tree_Size(node* root) {
		if (root == nullptr)
			return 0;
		else
			return Get_Tree_Size(root->right) + Get_Tree_Size(root->left) + 1;
	}

	//making array from the tree
	void Make_Array(node* root, int values[], int& size) {
		if (root == nullptr)
			return;
		Make_Array(root->left, values, size);
		values[size] = root->key;
		size++;
		Make_Array(root->right, values, size);
		return;
	}
};

#endif //TREE_ITERATOR_H