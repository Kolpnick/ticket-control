#include "TicketsLibrary.h"

extern "C" {
	//class Ticket_Amount C-wrapper
	Ticket_Amount* Create_Ticket_A(int id, int sell_time, int max_trips) {
		return new Ticket_Amount(id, sell_time, max_trips);
	}

	int Get_Max_Trips(Ticket_Amount* t) {
		return t->Get_Max_Trips();
	}
	
	int Get_Id_A(Ticket_Amount* t) {
		return t->Get_Id();
	}

	int Get_Sell_Time_A(Ticket_Amount* t) {
		return t->Get_Sell_Time();
	}

	int Get_Left_Trips(Ticket_Amount* t) {
		return t->Get_Left_Trips();
	}

	bool Try_Pass_Control_A(Ticket_Amount* t, int current_time) {
		return t->Try_Pass_Control(current_time);
	}

	void Print_A(Ticket_Amount* t) {
		t->Print();
	}

	void Dispose_Ticket_A(Ticket_Amount* t) {
		delete t;
	}

	//class Ticket_Period C-wrapper
	Ticket_Period* Create_Ticket_P(int id, int sell_time, int validity) {
		return new Ticket_Period(id, sell_time, validity);
	}

	int Get_Id_P(Ticket_Period *t) {
		return t->Get_Id();
	}

	int Get_Sell_Time_P(Ticket_Period* t) {
		return t->Get_Sell_Time();
	}

	int Get_Validity(Ticket_Period* t) {
		return t->Get_Validity();
	}

	bool Try_Pass_Control_P(Ticket_Period* t, int current_time) {
		return t->Try_Pass_Control(current_time);
	}

	void Print_P(Ticket_Period* t) {
		t->Print();
	}

	void Dispose_Ticket_P(Ticket_Period* t) {
		delete t;
	}

	//class Pass_Control C-wrapper
	Pass_Control* Create_Pass_Control(int current_time, Ticket* ticket) {
		return new Pass_Control(current_time, ticket);
	}

	int Get_Current_Time(Pass_Control* p) {
		return p->Get_Current_Time();
	}

	bool Try_Pass_Control(Pass_Control* p) {
		return p->Try_Pass_Control();
	}

	void Dispose_Control(Pass_Control* p) {
		delete p;
	}

	//class Known_Tickets C-wrapper
	Known_Tickets* Create_Tree() {
		return new Known_Tickets();
	}

	void Print_Tree(Known_Tickets* tree) {
		tree->Print_Tree();
	}

	int Add_Ticket(Known_Tickets* tree, Ticket* ticket) {
		return tree->Add_Ticket(ticket);
	}

	Ticket* Find_Ticket(Known_Tickets* tree, int id) {
		return tree->Find_Ticket(id);
	}

	int Control(Known_Tickets* tree, int current_time, Ticket* ticket) {
		return tree->Control(current_time, ticket);
	}

	bool Delete_Ticket(Known_Tickets* tree, Ticket* ticket) {
		return tree->Delete_Ticket(ticket);
	}

	void Dispose_Tree(Known_Tickets* tree) {
		return tree->~Known_Tickets();
	}

	//class Tree_Iterator C-wrapper
	Tree_Iterator* Create_Iterator(Known_Tickets* database) {
		return new Tree_Iterator(database);
	}

	//returning the next element in structure
	int Next(Tree_Iterator* iterator) {
		return iterator->Next();
	}

	//checking whether the next element exists
	bool Has_Next(Tree_Iterator* iterator) {
		return iterator->Has_Next();
	}

	void Dispose_Iterator(Tree_Iterator* iterator) {
		delete iterator;
	}
}