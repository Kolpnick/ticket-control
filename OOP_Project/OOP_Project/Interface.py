import ctypes
import datetime
lib_tickets = ctypes.CDLL('./libtickets.dll')


class Interface(object):
    # Class constructor
    def __init__(self, create_time):
        self.time = create_time
        self.date = datetime.date.today()
        self.last_id = 0
        self.tree = lib_tickets.Create_Tree()

    # Printing main menu
    def print_main_menu(self):
        print("MENU")
        print("1: Create and add ticket to the database")
        print("2: Delete ticket from the database")
        print("3: Get ticket information")
        print("4: Pass ticket control")
        print("5: Print database")
        print("6: Clear database")
        print("0: Exit\n")
        print("Select number between 0 and 6: ")
        self.handler_main_menu()

    # Printing create and add ticket to the database menu
    def create_ticket_menu(self):
        print("Select ticket type")
        print("1: Ticket for 1 / 10 / 30 / 60 trips")
        print("2: Ticket for a day / week / month")
        print("0: Return to the main menu\n")
        print("Enter your choice (1 / 10 / 30 / 60 / day / week / month / 0): ")
        self.handler_create_ticket_menu()

    # Printing select ticket type menu
    def select_ticket_menu(self):
        print("Select ticket type")
        print("1: Ticket for 1 / 10 / 30 / 60 trips")
        print("2: Ticket for a day / week / month")
        print("0: Return to the main menu\n")
        print("Select number between 0 and 2: ")
        return

    # Trying to find ticket in the database
    def ticket_id_handler(self):
        print("Enter ticket id: ")
        answer = input()
        if answer.isdigit():
            ticket = lib_tickets.Find_Ticket(self.tree, int(answer))
            if ticket:
                return ticket
            else:
                print("Ticket wasn't found!\n")
        else:
            print("Invalid ticket id. It must be a number > 0!\n")
        self.print_main_menu()

    # Processing user input in the main menu
    def handler_main_menu(self):
        answer = input()
        if answer.isdigit():
            answer = int(answer)
            if answer == 1:
                self.create_ticket_menu()
            if answer == 2:
                self.delete_ticket()
            if answer == 3:
                self.print_tinfo_handler()
            if answer == 4:
                self.control()
            if answer == 5:
                self.print_database()
            if answer == 6:
                self.clear_database()
            if answer == 0:
                lib_tickets.Dispose_Tree(self.tree)
                quit()
        print("Select number between 0 and 6: ")
        self.handler_main_menu()

    # Processing user input in the create ticket menu
    def handler_create_ticket_menu(self):
        answer = input()
        if answer.isdigit():
            answer = int(answer)
            if (answer == 1) or (answer == 10) or (answer == 30) or (answer == 60):
                self.create_ticket_a(answer)
            if answer == 0:
                self.print_main_menu()
        else:
            if answer == "day":
                self.create_ticket_p(1)
            if answer == "week":
                self.create_ticket_p(7)
            if answer == "month":
                self.create_ticket_p(30)
        print("Enter your choice (1 / 10 / 30 / 60 / day / week / month / 0): ")
        self.handler_create_ticket_menu()

    # Creating ticket for a certain number of trips
    def create_ticket_a(self, trips):
        self.get_current_time()
        self.last_id += 5
        ticket = lib_tickets.Create_Ticket_A(self.last_id, self.time, trips)
        lib_tickets.Add_Ticket(self.tree, ticket)
        print("Ticket was successfully created! Here ticket info:")
        self.print_ta_info(ticket)

    # Creating a valid ticket
    def create_ticket_p(self, period):
        self.get_current_time()
        self.last_id += 6
        ticket = lib_tickets.Create_Ticket_P(self.last_id, self.time, period)
        lib_tickets.Add_Ticket(self.tree, ticket)
        print("Ticket was successfully created! Here ticket info:")
        self.print_tp_info(ticket)

    # Getting real time and writing it into the class variable 'time'
    def get_current_time(self):
        if datetime.date.today().day - self.date.day > 0:
            self.time += datetime.date.today().day - self.date.day
        return

    # Deleting ticket from the database
    def delete_ticket(self):
        ticket = self.ticket_id_handler()
        lib_tickets.Delete_Ticket(self.tree, ticket)
        print("Ticket was successfully deleted!\n")
        self.print_main_menu()

    # Processing user input in the select ticket menu
    def print_tinfo_handler(self):
        self.select_ticket_menu()
        answer = input()
        if answer.isdigit():
            answer = int(answer)
            if answer == 1:
                ticket = self.ticket_id_handler()
                self.print_ta_info(ticket)
            if answer == 2:
                ticket = self.ticket_id_handler()
                self.print_tp_info(ticket)
            if answer == 0:
                self.print_main_menu()
        print("Select number between 0 and 2: ")
        self.print_tinfo_handler()

    # Printing information about a ticket for a certain number of trips
    def print_ta_info(self, ticket):
        print("Id:", lib_tickets.Get_Id_A(ticket))
        print("Sell time:", lib_tickets.Get_Sell_Time_A(ticket))
        print("Max trips:", lib_tickets.Get_Max_Trips(ticket))
        print("Left trips:", lib_tickets.Get_Left_Trips(ticket))
        print()
        self.print_main_menu()

    # Printing information about a valid ticket
    def print_tp_info(self, ticket):
        print("Id:", lib_tickets.Get_Id_P(ticket))
        print("Sell time:", lib_tickets.Get_Sell_Time_P(ticket))
        print("Valid period:", lib_tickets.Get_Validity(ticket))
        print()
        self.print_main_menu()

    # Passing control with ticket id
    def control(self):
        self.get_current_time()
        ticket = self.ticket_id_handler()
        can_pass = lib_tickets.Control(self.tree, self.time, ticket)
        if can_pass == 1:
            print("Access is allowed!\n")
        else:
            if can_pass == -2:
                print("Access denied! Ticket isn't valid.\n")
            else:
                print("Access denied! Ticket wasn't found.\n")
        self.print_main_menu()

    # Printing all tickets id
    def print_database(self):
        try:
            iterator = lib_tickets.Create_Iterator(self.tree)
        except WindowsError:
            print("\nDatabase is empty!\n")
            self.print_main_menu()
        print("\nAll Tickets")
        while lib_tickets.Has_Next(iterator):
            print("Id:", lib_tickets.Next(iterator))
        print()
        lib_tickets.Dispose_Iterator(iterator)
        self.print_main_menu()

    # Deleting all tickets from the database
    def clear_database(self):
        lib_tickets.Dispose_Tree(self.tree)
        self.tree = lib_tickets.Create_Tree()
        print("Database was successfully cleared!\n")
        self.print_main_menu()


# Creating and calling a class method that prints main menu
Menu = Interface(12)
Menu.print_main_menu()

